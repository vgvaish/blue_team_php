<?php

function getConn(){

	$db = parse_ini_file('config.ini', true);
        if($db['env'] == "prob"){
                $servername = $db['PRD_DB_HOST'];
                $username = $db['PRD_DB_USER'];
                $password = $db['PRD_DB_PASS'];
                $dbname = $db['PRD_DB_NAME'];
        }
        else{
                $servername = $db['DB_HOST'];
                $username = $db['DB_USER'];
                $password = $db['DB_PASS'];
                $dbname = $db['DB_NAME'];
        }

	$conn = null;

	try {
		$conn = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch(PDOException $e) {
		echo "Error: " . $e->getMessage();
	}

	return $conn;

}

function authorization() {
	session_start();
	if(!isset($_SESSION["username"])) {
		header("Location: login_form.php");
	}
}
getConn();
?>
