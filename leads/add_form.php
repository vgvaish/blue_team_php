<?php
require ("dbconn.php");
include("header.php");

?>
<html>
<head>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

</head>
<body class="d-flex flex-column h-100 container" style="background-image: url('https://www.google.com/url?sa=i&url=https%3A%2F%2Fwallpaperaccess.com%2Femployee&psig=AOvVaw1yhyPBfoBHKeDyaadM_bO5&ust=1634191278438000&source=images&cd=vfe&ved=0CAgQjRxqFwoTCIiTqY3bxvMCFQAAAAAdAAAAABAE');
    background-repeat: no-repeat; background-size: cover;">

        <h3 style="margin-top: 10px; font-style: italic; font-weight: bold;text-align: center;">Add Record</h3>

        <center>
		<form action = "add_action.php" method "POST"  style="margin-top: 20px; background-color: rgba(0, 0, 0, 0.6); padding: 40px;color: white; border-radius: 10px;max-width: 800px; text-align: left;" />

			<div class="mb-3">
                                <label for="name" class="form-label">Full Name</label>
                                <input type = "text" id="full_name" name = "full_name"  class="w-100 p-1" required autofocus placeholder = "FULL NAME.."/></div>
                         <div class="mb-3">
                                <label for="name" class="form-label">Phone Number</label>
				<input type = "text" name = "phone_number" class="w-100 p-1" required autofocus placeholder = "PHONE NUMBER.."/></div>
			<div class="mb-3">
                                <label for="name" class="form-label">Alt-Number</label>
				<input type = "text" name = "alt_number" class="w-100 p-1" required autofocus placeholder = "ALT NUMBER.."/></div>
			<div class="mb-3">
                                <label for="name" class="form-label">What's App Number</label>
				<input type = "text" name = "whatsapp_number" class="w-100 p-1" required autofocus placeholder = "WHATSAPP NUMBER.."/></div>
			<div class="mb-3">
                                <label for="name" class="form-label">Email_id</label>
				<input type = "text" name = "email_id" class="w-100 p-1" required autofocus placeholder = "EMAIL ID.."/></div>
			<div class="mb-3">
                                <label for="name" class="form-label">address</label>
				<input type = "text" id ="address" name = "address" class="w-100 p-1" required autofocus placeholder = "ADDRESS.."/></div>
			<div class="mb-3">
                                <label for="name" class="form-label">City</label>
				<input type = "text" id="city" name = "city" class="w-100 p-1" required autofocus placeholder = "CITY.."/></div>
			<div class="mb-3">
                                <label for="name" class="form-label">State</label>
				<input type = "text" name = "state" class="w-100 p-1" required autofocus placeholder = "STATE.."/></div>
			<div class="mb-3">
                                <label for="name" class="form-label">Pin Code</label>
				<input type = "text" name = "pincode" class="w-100 p-1" required autofocus placeholder = "PINCODE.."/></div>
		

			<button class="btn btn-success"  type="submit">Add</button>
                &nbsp;&nbsp;
			<button class="btn btn-dark" type="reset">Clear</button>
                &nbsp;&nbsp;
			<a href="list.php" class="btn btn-warning">Close</a>

</form>
</center>
</body>
</html>
