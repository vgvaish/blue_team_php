<?php

require('dbconn.php');

$arr = [];

$file_path = $_FILES["fileToUpload"]["tmp_name"];

//$file = fopen('customers_for_import.csv', 'r');
$file = fopen($file_path, 'r');


while (($line = fgetcsv($file)) !== FALSE) {
  $arr[] = $line;
}
$conn = getConn();

$insert_str = "";
foreach($arr as $rec) {
        $insert_str .= "(NULL, '" . implode("','", $rec) . "', NOW(), NOW(), 'open'), ";
}

$insert_str = rtrim($insert_str, ", ");

$sql = "INSERT INTO `leads` (`id`, `full_name`, `phone_number`, `alt_number`, `whatsapp_number`, `email_id`,`address`, `city`,`state`, `pincode` , `added_at`, `updated_at`, `status`) VALUES " . $insert_str;

error_log("==========DEBUG IMPORT SQL:======= $sql=======================");
$stmt = $conn->prepare($sql);
$stmt->execute();

header("Location: list.php?msg=Record added successfully");
