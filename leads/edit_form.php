<?php
require("dbconn.php");
include("header.php");

$id = $_REQUEST["id"];
$sql = "SELECT * FROM leads WHERE id = $id";
error_log($sql);

$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$record = $stmt->fetch();

?>
<html>
<head>
 <title>EDIT USER</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>
<body class="d-flex flex-column h-100 container" style="background-image: url('https://www.google.com/url?sa=i&url=https%3A%2F%2Fwallpaperaccess.com%2Femployee&psig=AOvVaw1yhyPBfoBHKeDyaadM_bO5&ust=1634191278438000&source=images&cd=vfe&ved=0CAgQjRxqFwoTCIiTqY3bxvMCFQAAAAAdAAAAABAE');
    background-repeat: no-repeat; background-size: cover;">
                <header>
                        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                          <div class="container-fluid">
                                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                                  </div>
                                </div>
                          </div>
                        </nav>
                </header>

                <h3 style="margin-top: 10px; font-style: italic; font-weight: bold;text-align: center;">Edit Record</h3>
                <center>

<form action="edit_action.php" method="POST" style="margin-top: 20px; background-color: rgba(0, 0, 0, 0.6); padding: 20px;color: white; border-radius: 10px;max-width: 800px; text-align: left;">

	<input type="hidden" name="id" value="<?php echo $record["id"] ?>"/>
        <div class="mb-3">
                <label for="full_name" class="form-label">Full_Name</label>
                <input type="text" name="full_name"  class="w-100 p-1" value="<?php echo $record["full_name"]; ?>"/></div>
	<div class="mb-3">
		<label for="phone_number" class="form_label">Phone_number</label>
		<input type="text" name="phone_number" class="w-100 p-1" value="<?php echo $record["phone_number"]; ?>"/></div>
	<div class="mb-3">
		<label for="alt_number" class="form_label">Alt_number</label>
		<input type="text" name="alt_number" class="w-100 p-1" value="<?php echo $record["alt_number"]; ?>"/></div>
	<div class="mb-3">
		<label for="whats_number" class="form_label">Whatsapp_number</label>
		<input type="text" name="whatsapp_number" class="w-100 p-1" value="<?php echo $record["whatsapp_number"]; ?>"/></div>
	<div class="mb-3">
		<label for"email_id" class="form_label">Email_id</label>
		<input type="text" name="email_id" class="w-100 p-1" value="<?php echo $record["email_id"]; ?>"/></div>
	<div class="mb-3">
 		<label for"address" class="form_label">Address</label>
		<input type="text" name="address" class="w-100 p-1" value="<?php echo $record["address"]; ?>"/></div>
	<div class="mb-3">
                <label for"city" class="form_label">City</label>
                <input type="text" name="city" class="w-100 p-1" value="<?php echo $record["city"]; ?>"/></div>
	<div class="mb-3">
		<label for"state" class="form_label">State</label>
		<input type="text" name="state" class="w-100 p-1" value="<?php echo $record["state"]; ?>"/></div>
	<div class="mb-3">
                <label for"pincode" class="form_label">Pincode</label>
		<input type="text" name="pincode" class="w-100 p-1" value="<?php echo $record["pincode"]; ?>"/></div>
 	<div class="mb-3">
                <label for"updated_at" class="form_label">Updated_at</label>
		<input type="text" name="updated_at" class="w-100 p-2" value="<?php echo $record["updated_at"]; ?>"/></div>
 	<div class="mb-3">
                <label for="status" class="form-label">STATUS</label>
		<select name="status">
                                <option value="open" <?php echo $record["status"] == 'open' ? 'selected' : ''; ?> > Open</option>
				<option value="close" <?php echo $record["status"] == 'close' ? 'selected' : ''; ?> > Close</option>
				<option value="surrender" <?php echo $record["status"] == 'surrender' ? 'selected' : ''; ?> > Surrender</option>
                       </select></div>

                <button class="btn btn-success" type="submit">Updated</button>&nbsp;&nbsp;
                <button class="btn btn-dark" type="reset">Clear</button>&nbsp;&nbsp;
                <a href="list.php" class="btn btn-warning">Close</a>
</form>
</html>
