<?php
require("dbconn.php");
$filename = 'leads_list.csv';

$sql = "SELECT * FROM leads";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$records = $stmt->fetchAll();
$file = fopen($filename, "w");

if($records > 0) {
foreach($records as $row){
        fputcsv($file, $row);

}
}
fclose($file);



header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=".$filename);
header("Content-Type: application/csv;");

readfile($filename);

unlink($filename);
exit();
