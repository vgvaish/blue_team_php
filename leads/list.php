<?php
require('dbconn.php');
include('header.php');

?>

<html>

	<head>
		<title>List Of Leads</title>
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">

                <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"crossorigin="anonymous"></script>



        </head>
        <body style="margin:20px">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                        <div class="container-fluid">
				<a class="btn btn-lg btn-primary" href="add_form.php">Add New</a>
				<!--<a href="export.php">Export Csv</a>-->
				<a href="importcsv_form.php" class="btn btn-lg btn-primary" >Imp Csv</a>
				 <form class="d-flex">
                                                <input class="form-control me-2" type="search" name="search" placeholder="Search" aria-label="Search">
                                                <button class="btn btn-outline-success" type="submit">Search</button>
                                        </form>
                        </div>
                </nav>
        <body> 
		 <table class="table table-striped table-hover text-center">
                        <thead>
                                <th>Id</th>
                                <th>Full_Name</th>
                                <th>Phone_number</th>
                                <th>Email_id</th>
                                <th>City</th>
                                <th>pincode</th>
                                <th>Added_at</th>
                                <th>updated_at</th>
                                <th>Status</th>
                                <th>Action</th>
                        </thead>
                        <tbody>

                                <?php foreach($records as $row) { ?>

                                        <tr>

                                                <td><?php echo $row['id']; ?></td>
                                                <td><?php echo $row['full_name']; ?></td>
						<td><?php echo $row['phone_number']; ?></td>
                                                <td><?php echo $row['email_id']; ?></td>
                                                <td><?php echo $row['city']; ?></td>
                                                <td><?php echo $row['pincode']; ?></td>
                                                <td><?php echo $row['added_at']; ?></td>
                                                <td><?php echo $row['updated_at']; ?></td>
                                                <td><?php echo $row['status']; ?></td>
                                                <td>
                                                 <button onclick="showUser(<?php echo $row['id']; ?>)" class="btn btn-info" data-bs-toggle="modal" data-bs-target="#exampleModal"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-eye" viewBox="0 0 16 16">
  <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z"/>
  <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z"/>
</svg></button>&nbsp;| &nbsp;
                                                        <a href="edit_form.php?id=<?php echo $row["id"]; ?>"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">  <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/><path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1.5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/></svg></a>
                                                        &nbsp;| &nbsp;
                                                        <a href="delete.php?id=<?php echo $row["id"]; ?>" onclick="return confirm('Are you sure you want to delete this record?');"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-trash-fill" viewBox="0 0 16 16"> <path d="M2.5 1a1 1 0 0 0-1 1v1a1 1 0 0 0 1 1H3v9a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V4h.5a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H10a1 1 0 0 0-1-1H7a1 1 0 0 0-1 1H2.5zm3 4a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 .5-.5zM8 5a.5.5 0 0 1 .5.5v7a.5.5 0 0 1-1 0v-7A.5.5 0 0 1 8 5zm3 .5v7a.5.5 0 0 1-1 0v-7a.5.5 0 0 1 1 0z"/>
</svg></a></td>

                                        </tr>

				<?php } ?>
 </tbody>
                </table>
        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">User Details</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
                FULL NAME : <span id="full_name"></span><br>
                Phone_number : <span id="phone_number"></span><br>
                Alter Number : <span id="alt_number"></span><br>
                WhatsApp Number : <span id="whatsapp_number"></span><br>
                Email_id : <span id="email_id"></span><br>
                Address : <span id="address"></span><br>
                City : <span id="city"></span><br>
                State : <span id="state"></span><br>
                Pincode : <span id="pincode"></span><br>
                <!--Added_date : <span id="added_at"></span><br>
                Updated Date: <span id="updated_at"></span><br>-->
                Status : <span id="status"></span><br>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
</body>
 <script>

function showUser(id){
        url = `single_record.php?id=${id}`
        fetch(url)
        .then(data=>data.json())
        .then(data=> {
        document.getElementById('full_name').innerHTML = data['full_name'];
        document.getElementById('phone_number').innerHTML = data['phone_number'];
        document.getElementById('alt_number').innerHTML = data['alt_number'];
        document.getElementById('whatsapp_number').innerHTML = data['whatsapp_number'];
        document.getElementById('email_id').innerHTML = data['email_id'];
        document.getElementById('address').innerHTML = data['address'];
        document.getElementById('city').innerHTML = data['city'];
        document.getElementById('state').innerHTML = data['state'];
        document.getElementById('pincode').innerHTML = data['pincode'];
        //document.getElementById('added_at').innerHTML = data['added_at'];
        //document.getElementById('updated_at').innerHTML = data['updated_at'];
        document.getElementById('status').innerHTML = data['status'];
}
      );
}
                </script>
</html>
