<?php
require("dbconn.php");
$filename = 'customers_list.csv';

$sql = "SELECT * FROM customers";
$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$records = $stmt->fetchAll();
$file = fopen($filename, "w");
//fputcsv($output, array('id', 'full_name', 'phone_number', 'alt_number', 'whatsapp_number', 'email_id', 'address', 'city', 'state', 'total_cost', 'paid', 'added_at', 'updated_at'));

if($records > 0) {
foreach($records as $row){
        fputcsv($file, $row);

}
}
fclose($file);



header("Content-Description: File Transfer");
header("Content-Disposition: attachment; filename=".$filename);
header("Content-Type: application/csv;");

readfile($filename);

unlink($filename);
exit();
