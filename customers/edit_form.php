<?php

require("dbconn.php");
include("header.php");

//authorization();
$id = $_REQUEST["id"];
$sql = "SELECT * FROM customers WHERE id = $id";
error_log($sql);

$stmt = $conn->prepare($sql);
$stmt->execute();
$result = $stmt->setFetchMode(PDO::FETCH_ASSOC);
$record = $stmt->fetch();
//print_r($record);

?>
<html>
<head>
        <title>EDIT USER</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
</head>
        <body class="d-flex flex-column h-100 container" style="background-image: url('https://www.google.com/url?sa=i&url=https%3A%2F%2Fwallpaperaccess.com%2Femployee&psig=AOvVaw1yhyPBfoBHKeDyaadM_bO5&ust=1634191278438000&source=images&cd=vfe&ved=0CAgQjRxqFwoTCIiTqY3bxvMCFQAAAAAdAAAAABAE');
    background-repeat: no-repeat; background-size: cover;">
                <header>
                        <nav class="navbar navbar-expand-lg navbar-light bg-light">
                          <div class="container-fluid">
                                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                                  </div>
                                </div>
                          </div>
                        </nav>
                </header>
		<h3 style="margin-top: 10px; font-style: italic; font-weight: bold;text-align: center;">Edit Record</h3>
		<form action="edit_action.php" method="POST" style="margin-top: 20px; background-color: rgba(0, 0, 0, 0.6); padding: 20px;color: white; border-radius: 10px;max-width: 800px; text-align: left;">
		
			<input type="hidden" name="id" required value="<?php echo $record["id"] ?>"/>
		<div class="mb-3">
			<label for="name" class="form-label">Full Name</label>
			<input type="text" class ="w-100 p-1" name="full_name" required value="<?php echo $record["full_name"]; ?>"/></div>
		<div class="mb-3">
                	<label for="name" class="form-label">Phone Number</label>
			<input type="text" class ="w-100 p-1" name="phone_number" required value="<?php echo $record["phone_number"]; ?>"/></div>
		<div class="mb-3">
			<label for="name" class="form-label">Alt-Number</label>
			<input type="text" name="alt_number" class ="w-100 p-1" required value="<?php echo $record["alt_number"]; ?>"/></div>
		<div class="mb-3">
			<label for="name" class="form-label">What's App Number</label>
			<input type="text" name="whatsapp_number" class ="w-100 p-1" required value="<?php echo $record["whatsapp_number"]; ?>"/></div>

		<div class="mb-3">
			<label for="name" class="form-label">Email-Id</label>
			<input type="text" name="email_id" class ="w-100 p-1" required value="<?php echo $record["email_id"]; ?>"/></div>
		<div class="mb-3">
			<label for="name" class="form-label">Address</label>
			<input type="text" name="address" class ="w-100 p-1" required value="<?php echo $record["address"]; ?>"/></div>
		<div class="mb-3">
			<label for="name" class="form-label">City</label>
			<input type = "text"name="city" class ="w-100 p-1" required value = "<?php echo $record["city"]; ?>" ></div>
		<div class="mb-3">
			<label for="name" class="form-label">State</label>
			<input type="text" name="state" class ="w-100 p-1" required value="<?php echo $record["state"]; ?>"/></div>
		<div class="mb-3">
			<label for="name" class="form-label">Total-Cost</label>	
			<input type="text" name="total_cost" class ="w-100 p-1" required value="<?php echo $record["total_cost"]; ?>"/></div>
		<div class="mb-3">
			<label for="name" class="form-label">Paid</label>
			<input type="text" name="paid" class ="w-100 p-1" required value="<?php echo $record["paid"]; ?>"/></div>
		Status:<div><select name="status">
                                <option value="active" <?php echo $record["status"] == 'active' ? 'selected' : ''; ?> >Active</option>
                                <option value="inactive" <?php echo $record["status"] == 'inactive' ? 'selected' : ''; ?> >Inactive</option>
                                <option value="open" <?php echo $record["status"] == 'open' ? 'selected' : ''; ?> >Open</option>
                                <option value="close" <?php echo $record["status"] == 'close' ? 'selected' : ''; ?> >Close</option>
                                <option value="surrender" <?php echo $record["status"] == 'surrender' ? 'selected' : ''; ?> >Surender</option>
			</select></div>
        <br>

	<center>
		<input type="submit" value="Update" class="btn btn-success"/>&nbsp;&nbsp;
		<input type="reset" value="Clear" class="btn btn-dark"/>&nbsp;&nbsp;
		<a href="list.php" class="btn btn-warning">Close</a>
	</center>
	</form>
</html>
